import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Profile } from 'selenium-webdriver/firefox';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  profile$: Observable<Profile>;

  constructor(private _http: HttpClient) {}
  
  ngOnInit() {
    this.profile$ = this._http.get<Profile>("/assets/mocks/profile.json");
  }
}
