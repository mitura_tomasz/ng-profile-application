import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollbarModule } from 'ngx-scrollbar';

import { ProfileComponent } from './profile.component';
import { CommentsComponent } from './components/comments/comments.component';
import { SummaryComponent } from './components/summary/summary.component';

@NgModule({
  imports: [
    CommonModule,
    ScrollbarModule 
  ],
  declarations: [ProfileComponent, CommentsComponent, SummaryComponent],
  exports: [ProfileComponent]
})
export class ProfileModule { }
