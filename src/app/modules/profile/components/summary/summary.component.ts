import { Component, OnInit, Input } from '@angular/core';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  @Input() profile: Profile;
  isProfileLiked: boolean = false;
  isProfileFollowed: boolean = false;

  
  constructor() { }

  ngOnInit() {}


  onFollowButtonClick(): void {
    this.isProfileFollowed ? --this.profile.followers : ++this.profile.followers;
    this.isProfileFollowed = !this.isProfileFollowed;
  }

  onHeartIconClick(): void {
    this.isProfileLiked ? --this.profile.likes : ++this.profile.likes;
    this.isProfileLiked = !this.isProfileLiked;
  }

  getProfileButtonValue(): string {
    return this.isProfileFollowed ? 'UNFOLLOW': 'FOLLOW';
  }
}
