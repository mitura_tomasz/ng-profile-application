import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import { Profile } from 'src/app/models/profile';
import { SlideAnimation } from 'src/app/animations/slide.animation';
import { Comment } from 'src/app/models/comment'
import { SortingService } from 'src/app/services/sorting.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  animations: [SlideAnimation]
})
export class CommentsComponent implements OnInit {

  @Input() profile: Profile;
  displayComments: boolean;
  @Output() displayCommentsEmitter = new EventEmitter<boolean>();
  @ViewChild('NgListOfCommentsScrollbar') listOfCommentsScrollbar: any;


  constructor(private sortingService: SortingService) { }

  ngOnInit() {
    this.displayComments = true;
    this.displayCommentsEmitter.emit(true);
  }

  onDisplayCommentsClick(): void {
    this.displayComments = !this.displayComments;
    this.displayCommentsEmitter.emit(this.displayComments);
  }

  getDisplayCommentsState(): string {
    return this.displayComments ? 'show' : 'hide'
  }

  onAddCommentEnter(commentText: string): void {
    this.addComment(commentText);
    this.profile.comments = this.sortingService.sortByTime(this.profile.comments);
    this.listOfCommentsScrollbar.scrollToBottom(1000).subscribe()
  }

  addComment(commentText: string): void {
    let comment: Comment = {
      fullName: 'John Cena',
      time: 0,
      text: commentText
    }

    this.profile.comments.push(comment);
  }
}
