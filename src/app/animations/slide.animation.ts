import { trigger, state, style, transition, animate } from '@angular/animations';

export const SlideAnimation = trigger('slideAnimation',
  [
    state(
      'show',
      style({'max-height': '{{ height }}px', 'opacity': '1'}),
      { params: { height: '*' } }
    ),
    state(
      'hide',
      style({'max-height': '0px', 'opacity': '0'}),
      { params: { height: '0' } }
    ),
    transition('show <=> hide', animate('350ms'))
  ]
);
