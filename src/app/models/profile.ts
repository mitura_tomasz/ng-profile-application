import { Comment } from './comment';

export interface Profile {
  fullName: string;
  town: string;
  country: string;
  likes: number;
  following: number;
  followers: number;
  comments: Comment[];
}
