export interface Comment {
  fullName: string;
  time: number;
  text: string;
}
